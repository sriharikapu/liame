import Vue from 'vue'
import Router from 'vue-router'
import MyPrefs from '../components/MyPrefs.vue'
import Dapp from '../components/Dapp.vue'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Dapp',
    component: Dapp
  },
  {
    path: '/prefs',
    name: 'MyPrefs',
    component: MyPrefs
  },
]

export default new Router({
  routes
})
