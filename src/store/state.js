export default {
  account: null,
  hasProvider: false,
  connected: false,
  verifierId: '',
  selectedVerifier: 'google',
  buildEnv: 'production',
  showModal: false,
  categories: ["urgent", "quarterly"]
}
