export default {
  account: state => state.account,
  hasProvider: state => state.hasProvider,
  connected: state => state.connected,
  verifierId: state => state.verifierId,
  selectedVerifier: state => state.selectedVerifier,
  buildEnv: state => state.buildEnv,
  showModal: state => state.showModal,
  categories: state => state.categories,
}
