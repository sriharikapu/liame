// import EmbarkJS from 'embark/EmbarkJS';
import EmbarkJS from "../embarkArtifacts/embarkjs"

// import contract
//import SimpleStorage from 'Embark/contracts/SimpleStorage';

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  store,
  render: function (h) {
    return h(App);
   }
})
