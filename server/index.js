import express from 'express'
import cors from 'cors'
import directSend from './modules/emailDirectSend'
import user from './user'
const app = express()
app.use(express.json())
app.use(cors())
require('dotenv').config()

const port = process.env.PORT || 3000

if (process.env.NODE_TYPE === 'dapp') {
  // context is external calls to trigger email from blockchain and directly
  const blockchains = require('./blockchains')

  // Send one time hash, email content
  app.post('/trigger/email', async (req, res) => {
    const data = req.body
    // console.log('data', data)
  
    // REQUIRED STUFF
    if (
      !data ||
      !data.to ||
      !data.from ||
      !data.subject ||
      !data.prefsAddress ||
      !data.category
    )
      throw new Error('Required fields missing: to, from, subject, prefsAddress')

    const options = {
      to: data.to,
      from: data.from,
      subject: data.subject,
      prefsAddress: data.prefsAddress,
      contractAddress: data.from.split('@')[0],
      category: data.category,
    }

    if (data.text) options.text = data.text
    if (data.html) options.html = data.html

    try {
      const result = await directSend.immediate(options)
      console.log('res', result)
    } catch(e) {
      console.log('e', e)
    }

    res.send('Sent Email!')
  })
} else {
  // context is only the SMTP server, and user account managed
  const emails = require('./emails')

  // send temporary auth credentials to return encrypted inbox
  app.post('/wallet/inbox', async (req, res) => {
    const inbox = await user.getInbox(req.body)
    res.json(inbox)
    res.end()
  })

  // send auth unlocked email to store forwarding address. NOTE: Would love to not have to store this!
  app.post('/wallet/proxy', (req, res) => {
    user.setProxy(req.body)
    res.send('User wallet set forwarding email!')
  })
}

app.listen(port, () => console.log(`LIAME API STARTED: ${process.env.HOST}:${process.env.PORT}`))
