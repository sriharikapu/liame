const axios = require('axios')
const crypto = require('crypto')
const nodemailer = require('nodemailer')
require('dotenv').config()

function sendGiftEmail(txn) {
  // NOTE: Generates a TESTING One Time Hash
  const oneTimeHash = contractAddress => {
    const uuid = crypto.randomBytes(16).toString("hex")
    // console.log('uuid, contractAddress', uuid, contractAddress)
    return uuid
  }

  const accountAddress = txn.from
  const otHash = oneTimeHash(accountAddress)
  // const tsts = await r.get(otHash)
  // console.log('tsts', tsts)
  // console.log('otHash', otHash)

  let transporter = nodemailer.createTransport({
    host: `${process.env.EMAIL_HOST}`,
    port: process.env.EMAIL_PORT,
    auth: {
      type: 'custom',
      method: 'XBLOCKCHAIN',
      user: accountAddress,
      pass: otHash
    },
    secure: false,
    customAuth: {
      XBLOCKCHAIN: async ctx => {
        // TODO: check the hash given by the contract against blockchain
        // console.log('ctx.auth', ctx.auth)
        if (
          !ctx.auth.credentials.user ||
          !ctx.auth.credentials.pass
        ) return ctx.reject(new Error('Invalid username or password'))

        return true
      }
    },
    logger: true,
    debug: false, // include SMTP traffic in the logs
    tls: {
      rejectUnauthorized: false
    },
  })

  let message = {
    from: `<${accountAddress}@liame.app>`,
    to: `<${otHash}@liame.app>`,
    subject: 'New Gift For You! - Kame.gg',
    text: 'You have received a new gift, please go to kame.gg to redeem this unique NFT!',
    html: '<h3>You have received a new gift!</h3><br />Please go to kame.gg to redeem this unique NFT!<br />http://kame.gg<br /><br /><br />This application runs on the Quorum blockchain.',
  }

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log('Error occurred')
      console.log(error.message)
      return process.exit(1)
    }
    // console.log('info', info)
    console.log('Message sent successfully!')

    // only needed when using pooled connections
    transporter.close()
    // process.exit()
  })
}




const KAME_PROD_RPC_HOST = '157.245.170.238'
const KAME_PROD_RPC_PORT = '22000'
const KAME_PROD_CONTRACT = '0x3359b4699768A13Aa8F977b7768309052721EcC2'.toLowerCase()
const KAME_STAGE_RPC_HOST = '157.245.167.80'
const KAME_STAGE_RPC_PORT = '22000' // 22001, 22002
const KAME_STAGE_CONTRACT = '0xfE09A873220604C80342CAf612dDF9EE6aAfF718'.toLowerCase()
const ACTIVE_CONTRACT = KAME_PROD_CONTRACT
// const ACTIVE_CONTRACT = KAME_STAGE_CONTRACT

const serverUrl = `http://${KAME_PROD_RPC_HOST}:${KAME_PROD_RPC_PORT}`
// const serverUrl = `http://${KAME_STAGE_RPC_HOST}:${KAME_STAGE_RPC_PORT}`
const blockHeightPayload = {"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":1}
const blockTxnsPayload = hash => ({"jsonrpc":"2.0","method":"eth_getBlockByNumber","params": [hash,true],"id":1})
const blockLogsPayload = hash => ({"jsonrpc":"2.0","method":"eth_getLogs","params":[{"blockHash": hash}],"id":1})
const pollInterval = 1500

const getBlockHeight = async () => {
  return axios.post(serverUrl, blockHeightPayload)
}
const getBlockTxns = async hash => {
  return axios.post(serverUrl, blockTxnsPayload(hash))
}

// Hacky loop with Quorum RPC
async function listenToBlocks() {
  const blRes = await getBlockHeight()
  const height = blRes.data
  console.log('height', height)
  const blockRes = await getBlockTxns(height.result)
  const block = blockRes.data.result
  if (block && block.transactions && block.transactions.length > 0) {
    // Filter the transaction to this contract, and send email about Gift!
    block.transactions.forEach(txn => {
      if (txn.to.toLowerCase() === ACTIVE_CONTRACT) {
        // Send gift email!
        sendGiftEmail(txn)
      }
    })
  }

  // repeat loop
  setTimeout(() => {
    listenToBlocks()
  }, pollInterval)
}

// initialize listener
listenToBlocks()
