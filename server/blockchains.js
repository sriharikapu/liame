import dotenv from 'dotenv'
import websocketHandlers from './modules/websocketHandlers'
import txnHandlers from './modules/txnHandlers'
dotenv.config('../.env')

const wsCallback = (type, data) => {
  // console.log('wsCallback data', type.eventName, data.contractAddress)
  txnHandlers.sortTxns(type.eventName, data)
}

// txnHandlers.filterThenSend('transaction', { contractAddress: '0xb8819c3edbb1a7779ffdfc6a77c6c1f9013b6896' })
// txnHandlers.filterThenSend('transaction', { contractAddress: '0xdac17f958d2ee523a2206206994597c13d831ec7', input: '0xa9059cbb00000000000000000000000068acdff8f6c788b9fd52af63912d9d50c095bb8100000000000000000000000000000000000000000000000000000002352bd680' })

websocketHandlers.init({
  apiKey: process.env.AMBERDATA_API_KEY,
  blockchainId: process.env.AMBERDATA_BLOCKCHAIN_ID,
  events: [
    { eventName: 'transaction' },
    // { eventName: 'internal_message' },
    // { eventName: 'internal_message', filters: { signature: '0x88c2a0bf'}},
  ],
}, wsCallback)
