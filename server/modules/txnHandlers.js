import fs from 'fs'
import path from 'path'
import abiDecoder from 'abi-decoder'
import Web3Data from 'web3data-js'
import triggerEmailHandlers from './triggerEmailHandler'
import UserPrefs from '../../embarkArtifacts/contracts/UserPrefs.js'
require('dotenv').config()

// Useful for testing things
let supportedContractAbis = [
  '0xdac17f958d2ee523a2206206994597c13d831ec7', // Tether
  '0xa74476443119a942de498590fe1f2454d7d4ac0d' // GNT
]

// NOTE: For demonstration purposes this is higher so we actually get a trigger
const lowBalanceThreshold = 1000 * 1e18
const lowBalanceContracts = [
  // Chainlink low balance alert addresses:
  '0x514910771af9ca656af840dff83e8264ecf986ca', // LINK Token
  '0x7a9d706b2a3b54f7cf3b5f2fcf94c5e2b3d7b24b', // Oracle Contract
  '0x79febf6b9f76853edbcbc913e6aae8232cfb9de9' // Oracle Contract
]

supportedContractAbis = supportedContractAbis.concat(lowBalanceContracts)

const getAbiFromAddress = async address => {
  const a = `${address}`.toLowerCase()
  // console.log('getAbiFromAddress', supportedContractAbis.includes(a), a)
  if (!supportedContractAbis.includes(a)) return
  const file = path.join(__dirname, '../../', 'test/abis', `${address}.json`)
  // console.log('file', file)
  const raw = await fs.readFileSync(file, 'utf8')
  if (!raw) return
  return JSON.parse(raw)
}

export default {
  sortTxns(type, data) {
    // return this.lowBalanceAlert(type, data)
    if (lowBalanceContracts.includes(data.to)) {
      // If chainlink address, check for low balance
      return this.lowBalanceAlert(type, data)
    } else {
      // Otherwise just filter for ABI methods
      return this.filterThenSend(type, data)
    }
  },

  async filterThenSend(type, data) {
    // Check if current type matches something we're looking for
    if (!type || type !== 'transaction') return Promise.resolve(`Event ${type} not supported`)
    if (!data || typeof data === 'undefined') return Promise.resolve('No data')
    // if (!data.contractAddress || typeof data.contractAddress === 'undefined') return Promise.resolve('No contractAddress')

    // Look for known ABIs, and then load within decoder
    let abi
    // if (data.contractAddress) abi = await getAbiFromAddress(data.contractAddress) // Check for new contract
    // if (!abi) abi = await getAbiFromAddress(data.from) // check for FROM contract
    // check for TO contract
    // NOTE: Only using "TO" here, since we just want to watch stuff happening to our dapp
    if (!abi) abi = await getAbiFromAddress(data.to)

    if (!abi) {
      // console.log('No ABI for:', data.contractAddress, data.from, data.to)
      return Promise.resolve()
    }
    abiDecoder.addABI(abi)

    // parse the data, based on our loaded ABI, otherwise just check for RAW configured 4byte signature
    const decodedData = abiDecoder.decodeMethod(data.input)
    // console.log('decodedData', decodedData, data)
    if (!decodedData) return Promise.resolve('No decoded data')

    // Cleanup when done:
    abiDecoder.removeABI(abi)

    // // Then, double check we have access before annoying :)
    // const hasAccess = await UserPrefs.methods.checkAccess(params.category, params.contractAddress).call()
    // if (!hasAccess) return Promise.reject('Access Denied')

    const foundValues = decodedData.params.map(d => {
      return `${d.name}: ${d.value}<br />`
    })

    try {
      // IF we have the right method, and data is ready to trigger, request One Time Hash
      // const otHash = '6805db8480ff4c73e0cc240cc673fda985729d5fcffbe1fbfcc30b027b2cc8ca@liame.app'
      const otHash = await UserPrefs.methods.getAccess('urgent', data.to).call()
      if (!otHash) return
      const packet = {
        address: data.contractAddress || data.from,
        message: {
          subject: 'Liame - Dapp Contract Update',
          text: `New message from: ${data.from} at block: ${data.blockNumber} with data: ${data.input}`,
          html: `New message!<br />from: ${data.from}<br />block: ${data.blockNumber}<br />method: ${decodedData.name}<br /><br />data:<br />${foundValues}`,
        },
        otHash,
      }
      // console.log('SENDING', packet)

      triggerEmailHandlers.send(packet)
    } catch (e) {
      console.log('otHash e', e)
    }
  },
  async lowBalanceAlert(type, data) {
    // Check if current type matches something we're looking for
    if (!type || type !== 'transaction') return Promise.resolve(`Event ${type} not supported`)
    if (!data || typeof data === 'undefined') return Promise.resolve('No data')
    // if (!data.contractAddress || typeof data.contractAddress === 'undefined') return Promise.resolve('No contractAddress')
    // console.log('data.to', data.to)

    // Look for known ABIs, and then load within decoder
    let abi
    // if (data.contractAddress) abi = await getAbiFromAddress(data.contractAddress) // Check for new contract
    // if (!abi) abi = await getAbiFromAddress(data.from) // check for FROM contract
    // check for TO contract
    // NOTE: Only using "TO" here, since we just want to watch stuff happening to our dapp
    if (!abi) abi = await getAbiFromAddress(data.to)

    if (!abi) {
      // console.log('No ABI for:', data.contractAddress, data.from, data.to)
      return Promise.resolve()
    }
    abiDecoder.addABI(abi)

    // parse the data, based on our loaded ABI, otherwise just check for RAW configured 4byte signature
    const decodedData = abiDecoder.decodeMethod(data.input)
    // console.log('decodedData', decodedData, data)
    if (!decodedData) return Promise.resolve('No decoded data')

    const w3d = new Web3Data(process.env.AMBERDATA_API_KEY)
    const balance = await w3d.address.getBalance(data.to, { includePrice: true, includeTokens: true })
    // console.log('balance', data.to, balance)

    // Filter for chainlink LINK balance
    let hasTrigger = false
    let triggerData
    if (balance && balance.tokens) {
      balance.tokens.forEach(token => {
        // Find LINK contract holdings
        if (token.address === lowBalanceContracts[0]) {
          // only email if LINK balance is below threshold!
          if (parseFloat(token.amount) < lowBalanceThreshold) {
            hasTrigger = true
            triggerData = token
          }
        }
      })
    }

    // Cleanup when done:
    abiDecoder.removeABI(abi)
    if (!hasTrigger) return

    // // Then, double check we have access before annoying :)
    // const hasAccess = await UserPrefs.methods.checkAccess(params.category, params.contractAddress).call()
    // if (!hasAccess) return Promise.reject('Access Denied')

    try {
      // IF we have the right method, and data is ready to trigger, request One Time Hash
      // const otHash = '6805db8480ff4c73e0cc240cc673fda985729d5fcffbe1fbfcc30b027b2cc8ca@liame.app'
      const otHash = await UserPrefs.methods.getAccess('urgent', data.to).call()
      if (!otHash) return
      const packet = {
        address: data.contractAddress || data.from,
        message: {
          subject: 'Liame - Low Balance Alert',
          text: `Low balance! Please add more funds to ${data.from}`,
          html: `<h2>Low ${triggerData.symbol} balance!</h2><br />Please add more ${triggerData.name} funds to ${data.from}.`,
        },
        otHash,
      }
      // console.log('SENDING', packet)

      triggerEmailHandlers.send(packet)
    } catch (e) {
      console.log('otHash e', e)
    }
  },
}
