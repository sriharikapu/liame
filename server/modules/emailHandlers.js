import redis from './redis'
const simpleParser = require('mailparser').simpleParser
// const StringDecoder = require('string_decoder').StringDecoder

redis.connect()

const defaultConfig = {
  secure: true,
  name: '', // optional hostname of the server, used for identifying to the client (defaults to os.hostname())
  banner: '', // optional greeting message. This message is appended to the default ESMTP response.
  size: '', // optional maximum allowed message size in bytes, see details here
  hideSize: true, // if set to true then does not expose the max allowed size to the client but keeps size related values like stream.sizeExceeded
  // socketTimeout: '', // how many milliseconds of inactivity to allow before disconnecting the client (defaults to 1 minute)
  // closeTimeout: '', // how many millisceonds to wait before disconnecting pending connections once server.close() has been called (defaults to 30 seconds)
  authMethods: ['LOGIN'],
  // authOptional: true,
  // allowInsecureAuth: true,
  tls: {
    rejectUnauthorized: false
  },
}

const onAuth = (auth, session, callback) => {
  // console.log('auth', auth)
  if (auth.method !== 'LOGIN') {
    // should never occur in this case as only XBLOCKCHAIN is allowed
    return callback(new Error("Expecting XBLOCKCHAIN auth fields"));
  }

  if (!auth.username || !auth.password) {
    return callback(new Error('No username or password sent!'))
  }

  callback(null, { user: auth.username })
}

// const onMailFrom = async (address, session, callback) => {
//   // // TODO: Change this to read from contract?
//   console.log('onMailFrom address', address)
//   return callback();
// }

const onRcptTo = async (address, session, callback) => {
  // TODO: Change this to read from contract and verify?
  const otHashStr = address.address.split('@')[0]
  const account = session.envelope.mailFrom.address.split('@')[0]
  // console.log('otHashStr', otHashStr)
  const hasHash = await redis.get(otHashStr)

  // if (address.address !== "demo@liame.app") {
  if (hasHash !== account) {
    return callback(
      new Error(`${account}@liame.app send mail prohibited without one time hash`)
    )
  }

  // Make certain it removed :)
  const fincheck = await redis.get(otHashStr)
  if (fincheck !== null) {
    return callback(
      new Error(`One time hash auth failed`)
    )
  }

  return callback()
}

const onConnect = (session, callback) => {
  if (process.env.NODE_ENV === 'production' && session.remoteAddress === "127.0.0.1") {
    return callback(new Error("No connections from localhost allowed"))
  }
  return callback()
}

const onData = async (stream, session, callback) => {
  stream.on('end', () => {
    let err
    if (stream.sizeExceeded) {
      err = new Error("Message exceeds fixed maximum message size")
      err.responseCode = 552
      return callback(err)
    }
    callback(null, "Message stored")
  })

  const parsed = await simpleParser(stream)
  const prefAddress = await redis.get(otHashStr)
  // Its only one time use after all!
  await redis.del(otHashStr)

  // Store the packet for later access! YAY
  redis.push(`${prefAddress}`, JSON.stringify({
    text: parsed.text,
    html: parsed.textAsHtml,
    subject: parsed.subject,
    date: parsed.date,
    to: parsed.to.text,
    from: parsed.from.text,
    messageId: parsed.messageId,
    isHtml: parsed.html,
  }))
}

const onClose = session => {
  console.log('onClose session', session)
}

export default {
  defaultConfig,
  onAuth,
  // onMailFrom,
  onRcptTo,
  onConnect,
  onData,
  // onClose,
}
