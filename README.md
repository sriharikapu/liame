![Liame.app](./static/Liame_app.jpg)

# Liame

Spam-free communications, where privacy and ownership are controlled by you and you alone.

### Setup

`npm i` - install it with eyes closed and hearts open :)

### Run

```
$ npm run serve:user // The server runs SMTP & authentication needs for individuals

$ npm run serve:dapp // The server runs generic SMTP send, and listens to configured blockchain contract events/transactions

$ npm run dev // The frontend, blockchain contracts & auto deployment for testing & development
```

#### Helper commands

`npm run testemail` - makes sure things are configured to send email via SMTP

`npm run testoth` - Tests one time hash email send

`npm run testquorum` - Tests email send for NFTs on Quorum

#### Important Notes

REDIS -- Redis is required for commands: `npm run serve:dapp`, `npm run serve:user` and `npm run testoth`. Make sure you have redis installed globally, then do `redis-server` in another terminal.

## Demo

![Home Page](./static/docs/homepage.png)
Demo Home Page

![Home Page](./static/docs/dapp_prefs.png)
Demo User Storing Preferences

##### Video Demos
See the video demo of basic user preferences stored on the blockchain dictating if they can be emailed or not: [View Video](./static/docs)

##### Low Balance Alerts
Imagine you deployed a contract that auto-pays for something, and you want to make sure it doesn't run out of funds. Liame allows you to set a threshold to get email alerted!
![Low Balance](./static/docs/lowbalance.png)

##### Quorum NFT Updates
So you built this really incredible platform that gifts NFTs to users when they share or participate in events, and you want to make sure they get these NFTs. Sometimes users have short attention spans, but now you can simply email the gift and they can redeem at their leasure!
![Quorum NFT](./static/docs/quorum_nft_gift.png)

NOTE: Thank you to the [@getKame](https://twitter.com/getKame) team for letting us use their Quorum RPC for testing! Check out their great app: https://kame.gg/
